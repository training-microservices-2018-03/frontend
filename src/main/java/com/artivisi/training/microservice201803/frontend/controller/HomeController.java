package com.artivisi.training.microservice201803.frontend.controller;

import com.artivisi.training.microservice201803.frontend.service.NasabahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @Autowired private NasabahService nasabahService;

    @GetMapping("/home")
    public ModelMap dashboard(){
        return new ModelMap("info", nasabahService.hostInfo());
    }
}
