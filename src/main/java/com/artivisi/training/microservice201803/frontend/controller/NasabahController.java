package com.artivisi.training.microservice201803.frontend.controller;

import com.artivisi.training.microservice201803.frontend.service.NasabahService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller @RequestMapping("/nasabah")
public class NasabahController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NasabahController.class);

    @Autowired private NasabahService nasabahService;

    @GetMapping("/list")
    public ModelMap semuaNasabah() {
        LOGGER.info("mengambil data nasabah");
        return new ModelMap()
                .addAttribute("daftarNasabah", nasabahService.semuaNasabah());
    }
}
