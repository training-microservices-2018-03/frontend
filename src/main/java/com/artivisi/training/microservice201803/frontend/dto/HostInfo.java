package com.artivisi.training.microservice201803.frontend.dto;

import lombok.Data;

@Data
public class HostInfo {
    private String hostname;
    private String port;
    private String ip;
}
