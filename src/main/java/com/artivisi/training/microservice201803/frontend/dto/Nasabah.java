package com.artivisi.training.microservice201803.frontend.dto;

import lombok.Data;

@Data
public class Nasabah {
    private String id;
    private String nomor;
    private String nama;
    private String email;
    private String noHp;
}
