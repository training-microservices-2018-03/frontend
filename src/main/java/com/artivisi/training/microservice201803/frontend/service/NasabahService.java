package com.artivisi.training.microservice201803.frontend.service;

import com.artivisi.training.microservice201803.frontend.dto.HostInfo;
import com.artivisi.training.microservice201803.frontend.dto.Nasabah;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "nasabah", fallback = NasabahServiceFallback.class)
public interface NasabahService {

    @GetMapping("/api/nasabah/")
    public Iterable<Nasabah> semuaNasabah();

    @GetMapping("/api/info")
    public HostInfo hostInfo();
}
