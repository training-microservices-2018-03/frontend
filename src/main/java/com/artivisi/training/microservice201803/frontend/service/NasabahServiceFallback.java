package com.artivisi.training.microservice201803.frontend.service;

import com.artivisi.training.microservice201803.frontend.dto.HostInfo;
import com.artivisi.training.microservice201803.frontend.dto.Nasabah;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class NasabahServiceFallback implements NasabahService {

    @Override
    public Iterable<Nasabah> semuaNasabah() {
        return new ArrayList<>();
    }

    @Override
    public HostInfo hostInfo() {
        HostInfo kosong = new HostInfo();
        kosong.setHostname("Not Available");
        kosong.setIp("0.0.0.0");
        kosong.setPort("0000");
        return kosong;
    }
}
